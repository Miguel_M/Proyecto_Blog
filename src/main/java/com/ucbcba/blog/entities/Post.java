package com.ucbcba.blog.entities;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Set;

/**
 * Created by amolina on 19/09/17.
 */
@Entity
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @ManyToOne
    @JoinColumn(name= "user_id" )
    private User user;

    @NotNull
    @Size(min = 1, message = "El titulo debe se entre 1 y 20 palabras solamente",max = 20)
    private String title;
    @NotNull
    @Size(min = 1, message = "El texto debe se entre 1 y 100 palabras solamente",max = 100)
    private String text;
    @NotNull
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date date=Date.valueOf(LocalDate.now());



    @OneToMany(mappedBy = "post")
    private Set<Comment> comments;


    @NotNull
    @Column(columnDefinition="int(5) default '0'")
    private Integer likes = 0 ;


    @ManyToOne
    @JoinColumn(name="category_id")
    private Category category;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public Set<Comment> getComments() {
        return comments;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setDate(LocalDate date){
        this.date=Date.valueOf(date);}

    public LocalDate getDate() {
        return date.toLocalDate();
    }
}

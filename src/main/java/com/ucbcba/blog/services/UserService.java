package com.ucbcba.blog.services;

import com.ucbcba.blog.entities.User;

public interface UserService {

    Iterable<User> listAllUser();

    User getUserById(Integer id);

    User saveUser(User user);

    void deleteUser(Integer id);

}

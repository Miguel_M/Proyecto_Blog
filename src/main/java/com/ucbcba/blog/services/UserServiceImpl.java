package com.ucbcba.blog.services;

import com.ucbcba.blog.entities.Post;
import com.ucbcba.blog.entities.User;
import com.ucbcba.blog.repositories.PostRepository;
import com.ucbcba.blog.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService{
    private UserRepository userRespository;

    @Autowired
    @Qualifier(value = "userRepository")
    public void setUserRepository(UserRepository userRepository) {
        this.userRespository = userRepository;
    }

    @Override
    public Iterable<User> listAllUser() {
        return userRespository.findAll();
    }

    @Override
    public User getUserById(Integer id) {
        return userRespository.findOne(id);
    }

    @Override
    public User saveUser(User user) {
        return userRespository.save(user);
    }

    @Override
    public void deleteUser(Integer id) {
        userRespository.delete(id);
    }
}

INSERT INTO `post`.`user` (`id`, `name`) VALUES ('1', 'Miguel');
INSERT INTO `post`.`user` (`id`, `name`) VALUES ('2', 'Valeria');
INSERT INTO `post`.`user` (`id`, `name`) VALUES ('3', 'Alfonso');

INSERT INTO `post`.`category` (`id`, `name`) VALUES ('1', 'Deportes');
INSERT INTO `post`.`category` (`id`, `name`) VALUES ('2', 'Ciencia');
INSERT INTO `post`.`category` (`id`, `name`) VALUES ('3', 'Arte');

INSERT INTO `post`.`post` (`id`, `likes`, `text`, `title`, `category_id`, `user_id`, `date`) VALUES ('1', '0', 'Texto 1', 'Titulo 1', '1', '1', '2017-10-04');
INSERT INTO `post`.`post` (`id`, `likes`, `text`, `title`, `category_id`, `user_id`, `date`) VALUES ('2', '0', 'Texto 2', 'Titulo 2', '2', '2', '2017-10-04');

INSERT INTO `post`.`comment` (`id`, `date`, `likes`, `text`, `post_id`) VALUES ('1', '2017-10-04', '0', 'Comentario 1', '1');
INSERT INTO `post`.`comment` (`id`, `date`, `likes`, `text`, `post_id`) VALUES ('2', '2017-10-04', '0', 'Comentario 2', '1');

INSERT INTO `post`.`comment` (`id`, `date`, `likes`, `text`, `post_id`) VALUES ('3', '2017-10-04', '0', 'Comentario 1', '2');
INSERT INTO `post`.`comment` (`id`, `date`, `likes`, `text`, `post_id`) VALUES ('4', '2017-10-04', '0', 'Comentario 2', '2');

